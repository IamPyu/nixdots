{lib, configs, pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    tmux fastfetch libvterm cava # terminal stuff

    # main
    xfce.thunar
    firefox
    kitty
    git curl stow

    # essential for system to work lol
    busybox file

    # programming
    gcc gdb gnumake cmake libtool valgrind # C
    sbcl roswell clojure leiningen # Clojure and Common Lisp
    rustc cargo # Rust
    zig # Zig
    go # Go
    nodejs # JS
    ldc dub # D

    # desktop
    swww hypridle hyprlock waybar rofi-wayland wl-clipboard fuzzel
    wireplumber pamixer brightnessctl swappy udiskie playerctl grim slurp libnotify pavucontrol
    networkmanagerapplet blueman bibata-cursors gnome.adwaita-icon-theme libadwaita
    teams-for-linux zathura obs-studio libresprite blockbench vesktop
    swaylock swayidle swaynotificationcenter
  
    # other
    ffmpeg htop

    # nix stuff
    home-manager
  ];

  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
  };

  programs.steam = {
    enable = true;
  };

  programs.zsh.enable = true;
  programs.firefox.enable = true;
  programs.dconf.enable = true;

  programs.sway = {
    enable = true;
    package = pkgs.swayfx;
    xwayland.enable = true;
  };
  xdg.portal.wlr.enable = true;

  services.flatpak.enable = true;
  services.upower.enable = true;

  fonts = {
    enableDefaultPackages = true;
    fontDir.enable = true;

    packages = with pkgs; [
      noto-fonts noto-fonts-cjk noto-fonts-emoji
      (nerdfonts.override {
        fonts = [
          "JetBrainsMono"
          "Iosevka"
        ];
      })
    ];
  };
}
