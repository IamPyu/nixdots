{ config, pkgs, inputs, ... }: {
  imports = [ 
    /etc/nixos/hardware-configuration.nix
    ./software.nix
    ./nixpkgs.nix
    ../modules/theme.nix
  ];

  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Networking
  networking.hostName = "pyu-machine";
  networking.networkmanager.enable = true;
  networking.firewall = {
    enable = true;
    allowedTCPPorts = [80 30000];
    allowedUDPPorts = [80 30000];
  };

  # Experimental features
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Locale and Timezone
  time.timeZone = "America/Regina";
  i18n.defaultLocale = "en_CA.UTF-8";

  # Xorg stuff
  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "";
  };

  # Printing
  services.printing.enable = true;

  # Audio
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # User
  users.users.pyu = {
    isNormalUser = true;
    initialPassword = "pyu";
    description = "pyu";
    shell = pkgs.zsh;
    extraGroups = [ "networkmanager" "wheel" ];
  };

  # Some hardware options
  hardware = {
    # opengl graphics
    opengl = {
      enable = true;
      
      driSupport = true;
      driSupport32Bit = true;

      extraPackages = with pkgs; [
        intel-media-driver
        libvdpau-va-gl
      ];
    };
    
    # bluetooth
    bluetooth = {
      enable = true;
      powerOnBoot = true;
    };
  };
  services.blueman.enable = true;

  environment.sessionVariables = {
    LIBVA_DRIVER_NAME = "iHD";
  };

  services.tlp = {
    enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

}
