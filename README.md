# nixdots

NixOS Dotfiles!

I have transferred fully to NixOS. Old dotfiles repo [here](https://gitlab.com/IamPyu/ndots)

## Configured Programs and Tools

- System-Wide Themes via [Stylix](https://github.com/danth/stylix)
- SwayFX
- Waybar
- Neovim
