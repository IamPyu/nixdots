set -xe
git add .

nix flake --experimental-features "nix-command flakes" update
sudo nixos-rebuild --impure --upgrade --flake .#pyu-machine switch
home-manager --experimental-features "nix-command flakes" --flake .#pyu switch

git commit -m "$(nix-env --list-generations | grep current)"
