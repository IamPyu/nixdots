emulate sh -c 'source /etc/profile' # In case /etc/zsh/zprofile gets exploded for some reason lol

export PATH="$PATH:$HOME/nix/files/scripts"

[ -d "/usr/share/glib-2.0/schemas" ] && export GSETTINGS_SCHEMA_DIR="/usr/share/glib-2.0/schemas"
[ -d "$HOME/.local/bin" ] && export PATH="$PATH:$HOME/.local/bin"
[ -d "$HOME/.roswell/bin" ] && export PATH="$PATH:$HOME/.roswell/bin"
[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"

[ -z "$PS1" ] && return

# autoloads
autoload -U colors && colors
autoload -U promptinit && promptinit

rm -rf ${ZDOTDIR:-${HOME}}/.zcompdump* # remove zcompdumps
autoload -U compinit && compinit

# prompt
name="%{$fg[green]%}%n%{$reset_color%}"
host="%{$fg[blue]%}%m%{$reset_color%}"
dir="%{$fg[green]%}%~%{$reset_color%}"
tail="λ "

export PROMPT="[$name@$host $dir]$tail"
export PS2="$tail"

# keybinds

bindkey -M vicmd "?" history-incremental-search-backward
