do
  local opts = {
    sw = 2,
    sts = 2,
    expandtab = true,

    number = true,
    relativenumber = true,

    background = "dark",
    termguicolors = true
  }

  for k, v in pairs(opts) do
    vim.o[k] = v
  end
end

do
 vim.g.mapleader = " "

  local function leadermap(mode, lhs, rhs, opts)
    vim.keymap.set(mode, vim.g.mapleader..lhs, rhs, opts)
  end
  local keymap_opts = {
    silent = true,
    noremap = true
  }

  leadermap("n", "pf", "<cmd>Telescope fd<CR>", keymap_opts)
  leadermap("n", "pg", "<cmd>Telescope live_grep<CR>", keymap_opts)
  leadermap("n", "pc", "<cmd>Telescope git_commits<CR>", keymap_opts)
  leadermap("n", "pG", "<cmd>Telescope git_files<CR>", keymap_opts)
end


