local cmp = require("cmp")
cmp.setup({
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered()
  },
  mapping = cmp.mapping.preset.insert({
    ["<CR>"] = cmp.mapping.confirm({select = true}),
    ["<C-c>"] = cmp.mapping.abort(),
    ["<C-Space>"] = cmp.mapping.complete()
  }),
  sources = cmp.config.sources({
    {name = "nvim_lsp"},
    {name = "luasnip"},
    {name = "conjure"}
  }, {
    {name = "buffer"}
  })
})

require("neodev").setup({})
local lspconfig = require("lspconfig")

local servers = {
  lua_ls = {},
  nil_ls = {},
  ccls = {},
  rust_analyzer = {},
  gopls = {},
  zls = {},
  tsserver = {},
  eslint = {},
  serve_d = {}
}

for k, v in pairs(servers) do
  local opts = v or {}
  opts["capabilities"] = require("cmp_nvim_lsp").default_capabilities()

  lspconfig[k].setup(opts)
end

require("lspsaga").setup({
  ui = {
    code_action = ""
  }
})

