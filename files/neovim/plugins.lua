require("nvim-treesitter.configs").setup({
  ensure_installed = {},
  sync_install = false,
  auto_install = false,
  highlight = {
    enable = true
  }
})

require("Comment").setup({})
require("lualine").setup({})
require("which-key").setup({})
