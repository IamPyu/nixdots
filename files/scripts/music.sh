#!/bin/sh
PREFIX=$HOME/Music

if [[ -d $PREFIX ]]; then
	rm -rf $PREFIX
fi

mkdir -p $PREFIX
mkdir -p $PREFIX/music

grabSong(){
  url=$1
  out=$2

  yt-dlp $url -vx -o "$PREFIX/music/$out"
  ffmpeg -i "$PREFIX/music/$out.opus" "$PREFIX/music/$out.mp3"

	if [[ $? -eq 0 ]]; then
    rm -rf "$PREFIX/music/$out.opus"
  fi
}

# Vocaloid
grabSong 'https://www.youtube.com/watch?v=duPJqfKiA78' 'triple baka'
grabSong 'https://www.youtube.com/watch?v=LLjfal8jCYI' 'override'
grabSong 'https://www.youtube.com/watch?v=1Swg-aBO9eY' 'konton boogie'
grabSong 'https://www.youtube.com/watch?v=UnIhRpIT7nc' 'lagtrain'
grabSong 'https://www.youtube.com/watch?v=G5hScSFkib4' 'rainy boots'
grabSong 'https://www.youtube.com/watch?v=mFih47l1pVI' 'haru no sekibaku'
grabSong 'https://www.youtube.com/watch?v=DeKLpgzh-qQ' 'lost umbrella'
grabSong 'https://www.youtube.com/watch?v=b56xjtP6Qac' 'relayouter'
grabSong 'https://www.youtube.com/watch?v=ffGcA4Kj-rk' 'loop spinner'
grabSong 'https://www.youtube.com/watch?v=gIfm67RuAi0' 'nee nee nee'
grabSong 'https://www.youtube.com/watch?v=G2PDJTkFiA8' 'miku'
grabSong 'https://www.youtube.com/watch?v=O9eHRiaTuL4' 'corspe dance'
grabSong 'https://www.youtube.com/watch?v=NTrm_idbhUk' 'love me, love me, love me'
grabSong 'https://www.youtube.com/watch?v=19y8YTbvri8' 'mesmerizer'
grabSong 'https://www.youtube.com/watch?v=cQKGUgOfD8U' 'echo'
grabSong 'https://www.youtube.com/watch?v=E2oRrLUxWKo' 'ai no uta'
grabSong 'https://www.youtube.com/watch?v=ft97RCDnCaY' 'horror terror'
grabSong 'https://www.youtube.com/watch?v=nPF7lit7Z00' 'you are a worthless child'
grabSong 'https://www.youtube.com/watch?v=sjr-tPFADpM' 'brain revolution girl'
grabSong 'https://www.youtube.com/watch?v=LCOItseOsFE' 'siu'
grabSong 'https://www.youtube.com/watch?v=Yt5-aNnDvBE' 'angel92'
grabSong 'https://www.youtube.com/watch?v=EEk4JGzqoFg' 'im the rain'
grabSong 'https://www.youtube.com/watch?v=3NLsyjOH92k' 'kimi ni kaikisen'
grabSong 'https://www.youtube.com/watch?v=wdUCRDvFv3Q' 'secret elementary school student'
grabSong 'https://www.youtube.com/watch?v=-bt0IP16PZI' 'stop nagging me!'
grabSong 'https://www.youtube.com/watch?v=widZEAJc0QM' 'ievan polkka'
grabSong 'https://www.youtube.com/watch?v=6kiQuUJWHuE' 'circus panic!!!'

