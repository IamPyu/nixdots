#!/usr/bin/env sh
VOLUME="5"
BRIGHTNESS="10"

if [[ "$1" = "vol" ]]; then
    if [[ "$2" = "up" ]]; then
	pamixer -i $VOLUME
    elif [[ "$2" = "down" ]]; then
	pamixer -d $VOLUME
    elif [[ "$2" = "mute" ]]; then
	pamixer -t
    fi

    volume=`pamixer --get-volume`
    dunstify -u low -r 32432 -h "int:value:$volume" "Volume: $volume"
elif [[ "$1" = "bright" ]]; then
    if [[ "$2" = "up" ]]; then
	brightnessctl s "+${BRIGHTNESS}%"
    elif [[ "$2" = "down" ]]; then
	brightnessctl s "${BRIGHTNESS}%-" 
    fi
fi
