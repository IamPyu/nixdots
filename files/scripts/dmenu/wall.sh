#!/bin/sh
find ~/nix/files/walls -name "*.*" | rofi -dmenu | xargs -r sh ~/nix/files/scripts/wall.sh
