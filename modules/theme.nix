{config, pkgs, ...}: let
  nord = "share/themes/nord.yaml";
  gruber = "share/themes/gruber.yaml";
  tokyonight = "share/themes/tokyo-night-dark.yaml";
  everforest = "share/themes/everforest.yaml";
in {
  stylix.enable = true;
  stylix.base16Scheme = "${pkgs.base16-schemes}/${nord}";
  stylix.image = ../files/walls/murky_peaks.png;
  stylix.polarity = "dark";
  
  stylix.cursor.package = pkgs.bibata-cursors;
  stylix.cursor.name = "Bibata-Modern-Ice";
  stylix.cursor.size = 24;
  stylix.autoEnable = true;
  
  stylix.fonts = {
    monospace = {
      package = pkgs.nerdfonts.override {fonts = ["JetBrainsMono"];};
      name = "JetBrains Nerd Font Mono";
    };
    serif = {
      package = pkgs.dejavu_fonts;
      name = "DejaVu Serif";
    };
    sansSerif = {
      package = pkgs.dejavu_fonts;
      name = "DejaVu Sans";
    };

    sizes = {
      applications = 12;
      terminal = 12;
      desktop = 12;
      popups = 10;
    };
  }; 
}
