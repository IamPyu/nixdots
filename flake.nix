{
  description = "Pyu's NixOS Configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    stylix = {
      url = "github:danth/stylix";
    };
  };

  outputs = { self, nixpkgs, home-manager, stylix, ... }@inputs:
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    nixosConfigurations."pyu-machine" = nixpkgs.lib.nixosSystem {
      specialArgs = {inherit system inputs;};
      modules = [
        stylix.nixosModules.stylix
        ./nixos/configuration.nix
      ];
    };
    
    homeConfigurations."pyu" = home-manager.lib.homeManagerConfiguration {
      inherit pkgs;

      extraSpecialArgs = {inherit inputs;};
      
      modules = [
        stylix.homeManagerModules.stylix
        ./home/home.nix
      ];
    };
  };
}
