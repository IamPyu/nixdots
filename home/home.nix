{lib, config, pkgs, ...}: {
  home.username = "pyu";
  home.homeDirectory = "/home/pyu";

  home.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };

  home.packages = with pkgs; [
    neovide # Program I never use
  ];

  imports = [
    ../modules/theme.nix
    ./gtk-qt.nix
    ./waybar.nix
    ./gaming.nix
    ./neovim.nix
    ./sway.nix
  ];

  programs.git = {
    enable = true;
    userName = "IamPyu";
    userEmail = "pyucreates@outlook.com";
  };

  programs.kitty.enable = true;
  programs.foot = {
    enable = true;
    settings.main = {
      font = lib.mkForce "JetBrains Mono Nerd Font Mono:size=12";
    };
  };

  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    location = "center";
    cycle = true;
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autocd = true;
    defaultKeymap = "emacs";
    dotDir = ".config/zsh";

    shellAliases = {
      ls = "${pkgs.eza}/bin/eza -lah";
      ff = "${pkgs.fastfetch}/bin/fastfetch";
      rm = "rm -rI";
      tm = "tmux";
      vi = "nvim";
    };

    history = {
      path = "${config.home.homeDirectory}/.zsh_histroy";
      save = 10000;
      size = 10000;
    };

    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    
    initExtra = "${builtins.readFile ../files/rc.zsh}";
  };

  programs.tmux = {
    enable = true;
    mouse = true;
    prefix = "C-a";

    plugins = with pkgs.tmuxPlugins; [
      sensible
    ];

    extraConfig = ''
    bind "|" split-window -h
    bind "-" split-window -v
    unbind '"'
    unbind "%"

    set -g default-terminal "tmux-256color"
    set -ga terminal-overrides ",*256col*:Tc"
    set -ga terminal-overrides '*:Ss=\E[%p1%d q:Se=\E[ q'
    set-environment -g COLORTERM "truecolor" 
    '';
  };

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    nix-direnv.enable = true;
  };

  programs.fastfetch = {
    enable = true;
    settings = {
      logo = {
        source = "nixos_old_small";
      };

      modules = [
        "title"
        "os"
        "kernel"
        "host"
        "packages"
        "wm"
        "memory"
        "colors"
      ];
    };
  };
  programs.fuzzel.enable = true;

  home.stateVersion = "24.05";
}
