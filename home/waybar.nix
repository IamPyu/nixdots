{config, pkgs, ...}: {
  #xdg.configFile."waybar".source = ../files/waybar;

  programs.waybar = {
    enable = true;
    settings = {
      mainBar = {
        layer = "top";
        position = "top";
        spacing = 10;
        
        modules-left = [
          "hyprland/workspaces"
          "sway/workspaces"
          "wlr/taskbar"
        ];

        modules-center = [
          "clock"
        ];

        modules-right = [
          "backlight"
          "pulseaudio"
          "battery"
          "tray"
        ];
        
        tray.spacing = 10;
        clock = {
          format = "{:%a %b/%d | %I:%M %p}";
          interval = 5;
        };
        
        battery = {
          states = {
            good = 80;
            warning = 45;
            critical = 15;
          };
          
          
          format = "{capacity}% 󰁿";
          format-full = "{capacity}% 󱟢";
          format-charging = "{capacity}% 󱟦";
          format-plugged = "{capacity}% 󰂄";
          format-critical = "{capacity}% 󰂃";
        };
        
        pulseaudio = {
          scroll-step = 1;
          format = "{volume}% {icon}";
          format-muted = "Muted 󰖁";
          format-source = "{volume}%";
          format-icons = {
            default = ["" "" ""];
          };
          on-click = "pavucontrol";
        };
        
        backlight = {
          device = "intel_backlight";
          format = "{percent}% {icon}";
          format-icons = ["󰃞" "󰃟" "󰃠"];
        };

        "wlr/taskbar" = {
          on-click = "minimize-raise";
        };
      };
    };
  };
}
