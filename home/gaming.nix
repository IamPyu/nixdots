{config, pkgs, ...}: {
  home.packages = with pkgs; [
    prismlauncher papermc # Minecraft
    minetest # Minetest
  ];
}
