{config, pkgs, ...}: {
  xdg.configFile."sway".source = ../files/sway; 
  programs.swaylock.enable = true;
}
