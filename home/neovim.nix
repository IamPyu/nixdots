{config, pkgs, lib, ...}: {
  programs.neovim = {
    enable = true;

    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;

    extraLuaConfig = ''
      ${builtins.readFile ../files/neovim/plugins.lua}
      ${builtins.readFile ../files/neovim/cmp-lsp.lua}
      ${builtins.readFile ../files/neovim/config.lua}
    '';

    plugins = with pkgs.vimPlugins; [
      telescope-nvim
      luasnip
      friendly-snippets
      which-key-nvim
      oil-nvim
      direnv-vim

      (nvim-treesitter.withPlugins (p: with p; [
	tree-sitter-c
	tree-sitter-cpp
	tree-sitter-lua
	tree-sitter-nix
      ]))

      neodev-nvim 
      nvim-lspconfig
      lspsaga-nvim

      nvim-cmp
      cmp-nvim-lsp
      cmp_luasnip

      comment-nvim

      lualine-nvim
      nvim-web-devicons
    ];

    extraPackages = with pkgs; [
      ripgrep
      fd
      tree-sitter

      nil
      ccls
      rust-analyzer
      lua-language-server
      gopls
      zls


      serve-d

      nodePackages.typescript-language-server
    ];
  };
}
